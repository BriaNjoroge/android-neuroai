package com.example.neuro_symbolicai;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

//    @FormUrlEncoded
//    @POST("/results")
//    Call<ImageClass> uploadImage(@Field("question") String title,@Field("photo") String Image);

    @Multipart

    @POST("/results")

    Call<ImageClass> uploadImage(@Part("question") String description, @Part("photo")String Image);

}


