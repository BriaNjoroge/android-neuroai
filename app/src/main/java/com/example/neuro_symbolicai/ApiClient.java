package com.example.neuro_symbolicai;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BaseUrl = "https://9e0157d0.ngrok.io/";
    private static Retrofit retrofit;

    public static Retrofit getApiClient(){

        if(retrofit==null)
        {
            retrofit = new Retrofit.Builder().baseUrl(BaseUrl).
                    addConverterFactory(GsonConverterFactory.create()).build();
        }

        return retrofit;
    }
}
